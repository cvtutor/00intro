# 0. Intro
_Tutorials from "Introduction to OpenCV"._

Getting started with OpenCV C++ 3.1.x.

Note that the example codes are built on Windows platform (Win32).


    git clone https://github.com/cvtutor/intro.git 00intro


## OpenCV Doc

* [Introduction to OpenCV](http://docs.opencv.org/3.1.0/df/d65/tutorial_table_of_content_introduction.html)



### CVTutor.Intro.Hello01

* _Hello OpenCV!_

Usage:    
    `CVTutor.Intro.Hello01`

This tutorial teaches how to install pre-built OpenCV 3.1.0 binaries on Windows (Visual Studio 2015/2017).
After the tutorial, you should be able to build and run the proverbial "Hello OpenCV" app.


### CVTutor.Intro.Load01

* [Load and Display an Image](http://docs.opencv.org/3.1.0/db/deb/tutorial_display_image.html)

Usage:    
    `CVTutor.Intro.Load01 <image filename>`

How to load an image (into cv::Mat data structure) from a file.

	
### CVTutor.Intro.Save01

* [Load, Modify, and Save an Image](http://docs.opencv.org/3.1.0/db/d64/tutorial_load_save_image.html)

Usage:     
    `CVTutor.Intro.Save01 <image filename>`

How to save the OpenCV cv::Mat to a file.


### CVTutor.Intro.Capture01

* [cv::VideoCapture Class Reference](http://docs.opencv.org/master/d8/dfe/classcv_1_1VideoCapture.html)

Usage:    
    `CVTutor.Intro.Capture01 <video filename>`

This tutorial shows how to use the OpenCV class `VideoCapture` to load a video file.
Note that `VideoCapture` can be used to capture live video frames from connected cameras,
which will be explained in later tutorials.

